# hello-flask-py

A simple "hello-world" python rest app using flask.

## Features

- [X] run as local python rest app
- [X] run in docker container
- [ ] run in kubernetes cluster
