#!/usr/bin/env python
# hello-flask.py

from flask import Flask
app = Flask(__name__)

FLASK_DEFAULT_PORT = 5000

@app.route("/")
def hello():
    return "Hello from Python!"

if __name__ == "__main__":
    PORT = FLASK_DEFAULT_PORT
    print('Try ... http://localhost:' + str(PORT))
    app.run(host='0.0.0.0', debug=True, port=PORT)
